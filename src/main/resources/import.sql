INSERT INTO car (id, year, brand, color)
VALUES (1, 2014, 'vw', 'blue'),
       (2, 2013, 'peugeot', 'black'),
       (3, 2001, 'renault', 'green'),
       (4, 2016, 'vw', 'blue'),
       (5, 2014, 'mercedes', 'black'),
       (6, 2019, 'peugeot', 'gray'),
       (7, 2009, 'vw', 'gray');