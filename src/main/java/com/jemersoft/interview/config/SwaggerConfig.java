package com.jemersoft.interview.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {

    private static final String BASE_PACKAGE = "com.jemersoft.interview.web.rest";
    private static final String MESSAGE_400 = "Bad Request";
    private static final String MESSAGE_401 = "Unauthorized";
    private static final String MESSAGE_500 = "Internal Server Error";

    @Bean
    public static Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .useDefaultResponseMessages(false)
            .globalResponseMessage(RequestMethod.GET, globalResponseMessages());
    }

    private static ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("Jemersoft Interview API")
            .description("Jemersoft Interview API")
            .version("1.0.0")
            .termsOfServiceUrl("Terms of service URL")
            .license("License of API")
            .licenseUrl("API license URL")
            .build();
    }

    private static List<ResponseMessage> globalResponseMessages() {
        return Arrays.asList(
            new ResponseMessageBuilder()
                .code(HttpStatus.BAD_REQUEST.value())
                .message(MESSAGE_400)
                .build(),
            new ResponseMessageBuilder()
                .code(HttpStatus.UNAUTHORIZED.value())
                .message(MESSAGE_401)
                .build(),
            new ResponseMessageBuilder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(MESSAGE_500)
                .build());
    }
}
