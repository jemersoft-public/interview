package com.jemersoft.interview.web.rest;

import com.jemersoft.interview.domain.Car;
import com.jemersoft.interview.service.CarService;
import com.jemersoft.interview.util.ResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CarResource {

    private final CarService carService;

    public CarResource(CarService carService) {
        this.carService = carService;
    }

    @GetMapping("/cars")
    public ResponseEntity<List<Car>> getAllCars() {
        return ResponseEntity.ok().body(carService.findAllCars());
        //return new ResponseEntity<>(carService.findAllCars(), OK);
    }

    @GetMapping("/days/{id}")
    public ResponseEntity<Car> getCar(@PathVariable Long id) {
        return ResponseUtil.wrapOrNotFound(carService.findCar(id));
    }
}
