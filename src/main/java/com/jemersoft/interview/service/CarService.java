package com.jemersoft.interview.service;

import com.jemersoft.interview.domain.Car;
import com.jemersoft.interview.repository.CarRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CarService {

    private CarRepository carRepository;

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> findAllCars() {
        return carRepository.findAll();
    }

    public Optional<Car> findCar(Long carId) {
        return carRepository.findById(carId);
    }
}
