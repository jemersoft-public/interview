# Jemersoft Interview Api

Aplicación demo de Java / Sprint Boot / Srping JPA / MySql / Postman

## Prerrequisitos

Instalar MySql database (Solo es necesario el My Sql Server y Workbrench)

    https://dev.mysql.com/downloads/installer/

Instalar Java 8

    https://www.java.com/es/download/

Instalar Maven

    http://apache.dattatec.com/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip

Instalar Postman

    https://www.getpostman.com/downloads/

Instalar Git

    https://git-scm.com/downloads

## Importación en IDE

Importar el proyecto java de manera standard con maven

## Deploy

Correr el siguiente comando en el path del proyecto

    mvn spring-boot:run 

Una vez deployada la aplicación deberíamos poder obtener una respuesta a los siguientes endpoints utilizando postman:

    GET     http://localhost:8080/api/cars
    GET     http://localhost:8080/api/cars/1

## Objetivos

### 1 - Completar entidad Car

Como verificamos en la etapa de deploy, la entidad CAR, tiene solamente los endpoints GET para obetener todos y para obetner por id (/cars y /cars{id}).
El primer objetivo, sera completar la entidad CAR construyendo los siguientes 2 endpoints:

    POST    http://localhost:8080/api/cars
    DELETE  http://localhost:8080/api/cars/1

El primer endpoint de tipo POST, deberá recibir como para metro un objeto JSON similar al siguiente ejemplo y crear un nuevo registro en la base de datos. Al cual podrémos verificar a travez de la llamada GET /cars

    {
      "brand": "vw",
      "color": "red",
      "year": 2015
    }

El segundo endpoint de tipo DELETE, deberá borrar el objeto cuyo id es pasado como parametro. Por ejemplo, si llamamos a:

    DELETE http://localhost:8080/cars/1

Debería borrar el objeto CAR de id 1.

### 2 - Crear una nueva entidad Person

En este objetivo, deberíamos ser capaces de crear una nueva entidad llamada Person con los siguientes campos:
 
    name
    dni
    cars (Puede tener 1 o muchos autos)
    
con los mismos endpoints que teniamos en car:

    GET     http://localhost:8080/persons
    GET     http://localhost:8080/persons/1
    POST    http://localhost:8080/persons
    DELETE  http://localhost:8080/persons/1
 
 De esta manera, si llamamos al endpoint POST con el siguiente body
 
     {
       "name": "juan perez",
       "dni": 33333333,
       "cars": [
            {
              "brand": "mercedes",
              "color": "black",
              "year": 2018
            }
       ]
     }
 
 Deberia crear un objeto persona con un nuevo auto. Lo cual podemos verificar llamando a 
 
     GET     http://localhost:8080/persons
    
### 3 - Commit and Push

Finalmente, solo resta subir el codigo. Para esto, es necesario crear un nuevo branch "nombre-apellido" y luego subir el codigo.

Para crear un nuevo breanch, es necesario correr le siguiente comando

    git checkout -b "nombre-apellido"

Una vez creado el branch, agregar todos los cambios, hacer commit y luego push:

    git add -all
    git commit -m "jemersoft commit"
    git push
    

### 4 - BONUS - Crear Angular ABM para administrar autos o personas 

Para completar el flujo de la app, se podría crear las pantallas de para administrar los Cars y las Persons (Listado / Vista / Creado y Borrado)
